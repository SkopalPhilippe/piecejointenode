const express = require('express')
const mysql = require("mysql")
const formidable = require("formidable")
const { v4: uuidv4 } = require('uuid')
const fs = require('fs')
const fsPromises = fs.promises
const app = express()
const port = 3000

const connectionConfig = require('./database.js')
const connection = mysql.createConnection(connectionConfig)

app.use(express.urlencoded());
app.use(express.json());
app.set('view engine', 'pug')
app.use(express.static('assets'))
app.use(express.static('sentImages'))

const pathFileToUpload = "assets/sentImages"

// Init received images folder
createIfNotExist(pathFileToUpload)

/* 
  GET ROUTES
*/ 
app.get('/', async (req, res) => {
  const requests = await getAllRequest()
  res.render('list', {elements: requests, title: "List"})
}) 

app.get('/newRequest', (req, res) => {
  res.render('newRequest', {title: "New request"})
}) 

app.get('/request/:id', async (req, res) => {
  const result = await getRequest(req.params.id)
  const imagesList = await getImagesByReqId(req.params.id)

  const useableImagesList  = []
  for (let el of imagesList) {
    useableImagesList.push('/sentImages/' + el.imageName)
  }

  res.render('viewRequest', {el: result, title: "Request", imageList: useableImagesList})
}) 

app.get('/uploadFile/:id', async (req, res) => {
  const result = await getRequest(req.params.id)
  res.render('uploadFile', {el: result, title: "Upload file"})
}) 

app.get('/delRequest/:id', async (req, res) => {
  await delRequest(req.params.id)
  res.redirect('/')
})

/* 
  POST ROUTES
*/ 
app.post('/createRequest', async (req, res) => {
  await createRequest(req.body.type, req.body.comment)  
  res.redirect('/')
})

app.post('/uploadFile/:id', async (req, res) => {
  const fileExtensionPattern = /\.([0-9a-z]+)(?=[?#])|(\.)(?:[\w]+)$/gmi
  const form = new formidable.IncomingForm()
  form.parse(req, async function (err, fields, files) {
    const oldpath = files.filetoupload.path
    const newFileName  = uuidv4()
    const fileExtension = files.filetoupload.name.match(fileExtensionPattern)[0]
    const newpath = pathFileToUpload + "/" + newFileName + fileExtension
    try {
      fsPromises.rename(oldpath, newpath)
      await createImageRequestAssociation(req.params.id, newFileName + fileExtension)
      res.redirect(`/request/${req.params.id}`)
    } catch (err) {
      console.log(err)
    }
  })
}) 

/* 
  Functions
*/
async function createIfNotExist(path) {
  try {
    fsPromises.access(path, fs.constants.R_OK | fs.constants.W_OK)
      .catch(() => fsPromises.mkdir(path));
  } catch (err) {
    console.log(err)
  }
}


function getAllRequest() {
  return new Promise((resolve) => {
    connection.query("SELECT * from request", function (error, results, fields) {
      if (error) throw error;
      resolve(results)
    })
  }) 
}

function getRequest(id) {
  return new Promise((resolve) => {
    connection.query(`SELECT * from request where id=${id}`, function (error, results, fields) {
      if (error) throw error;
      resolve(results[0])
    })
  })
}

function delRequest(id) {
  return new Promise((resolve) => {
    connection.query(`delete from request where id=${id}`, function (error, results, fields) {
      if (error) throw error;
      resolve(results[0])
    })
  })
}

function createRequest(type, comment) {
  return new Promise((resolve) => {
    connection.query(`insert into request (type, comment) values('${type}', '${comment}')`, function (error, results, fields) {
      if (error) throw error;
      resolve(results[0])
    })
  })
}

async function createImageRequestAssociation(reqId, imageName) {
  return new Promise((resolve) => {
    connection.query(`insert into imagesRequest (requestId, imageName) values('${reqId}', '${imageName}')`, function (error, results, fields) {
      if (error) throw error;
      resolve(results[0])
    })
  })
}

async function getImagesByReqId(reqId) {
  return new Promise((resolve) => {
    connection.query(`SELECT imageName from imagesRequest where requestId=${reqId}`, function (error, results, fields) {
      if (error) throw error;
      resolve(results)
    })
  })
}



app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})